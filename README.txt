Installazione:
- importare il progetto in eclipse
- utilizzare il supporto ad Android 4.2.2
- eventualmente caricare manualmente le librerie contenute nella cartella /libs

Spiegazione utilizzo:
- installare l'applicazione sul dispositivo android
- automaticamente richiedera' di accendere il bluetooth se non acceso
- selezionare la frequenza e il tempo di acquisizione
- aprire il menu per procedere allo scan o alla lista dei dispositivi gia' associati e per associarvisi
- una volta connessi al server si caricheranno i dati che vengono plottati sul grafico
- una volta terminato si puo' premere esporta per esportare i dati ricevuti dall'ultimo device su un database sulla sdcard
