package com.team16.BTRD;



import android.app.Activity;
import android.graphics.Color;
import com.androidplot.xy.*;

 
/**
 * Class that manage the diagram generation
 */
public class DiagramDesign
{

    
    private static final int HISTORY_SIZE = 30;            // number of points to plot in history
    private XYPlot HistoryPlot = null;
    private SimpleXYSeries Series = null;


    /** Called when the activity is first created. */
    public DiagramDesign(Activity act) {
    	// Setp up the Serie
    	Series = new SimpleXYSeries("Heart Rate");
        Series.useImplicitXVals();
        // setup the History plot:
        HistoryPlot = (XYPlot) act.findViewById(R.id.diagram);
        
        // Configuring the History plot
        HistoryPlot.setRangeBoundaries(0, 200, BoundaryMode.FIXED);
        HistoryPlot.setDomainBoundaries(0, HISTORY_SIZE, BoundaryMode.FIXED);
        HistoryPlot.addSeries(Series, new LineAndPointFormatter(Color.rgb(255, 0, 0), Color.BLACK,null));
        HistoryPlot.setDomainStepValue(11);
        HistoryPlot.setTicksPerRangeLabel(1);
        HistoryPlot.setDomainLabel("Sample Index");
        HistoryPlot.getDomainLabelWidget().pack();
        HistoryPlot.setRangeLabel("BPM");
        HistoryPlot.getRangeLabelWidget().pack();
        HistoryPlot.getGraphWidget().getDomainLabelPaint().setTextSize(15);
        HistoryPlot.getGraphWidget().getDomainOriginLabelPaint().setTextSize(15);
        HistoryPlot.getGraphWidget().getRangeLabelPaint().setTextSize(15);
        HistoryPlot.getGraphWidget().getRangeOriginLabelPaint().setTextSize(15);
        HistoryPlot.getGraphWidget().setMargins(10, 10, 10, 10);     
    }

    /**
    * function called for the updating of the plot with the new value
    * @param str string that represent the new value that we need to plot
    */
    public synchronized void onDataChanged(String str) {
    	float value = Float.parseFloat(str);
        // get rid the oldest sample in history:
        if (Series.size() > HISTORY_SIZE) {
            Series.removeFirst();
        }
        // add the latest history sample:
        Series.addLast(null, value);
        // redraw the Plot:
        HistoryPlot.redraw();
    }
}