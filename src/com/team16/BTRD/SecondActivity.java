package com.team16.BTRD;

import com.team16.BTRD.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

/**
 * Class that implement the "Settings" Tab
 */
public class SecondActivity extends Activity {
	SeekBar seekbar, seekbar2;
	TextView value, value2;
	int progress = 1;
	int progress2 = 1;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.second);
		value = (TextView) findViewById(R.id.textview);
		seekbar = (SeekBar) findViewById(R.id.seekbar);
		value2 = (TextView) findViewById(R.id.textview2);
		seekbar2 = (SeekBar) findViewById(R.id.seekbar2);
		seekbar.setProgress(1);
		seekbar.incrementProgressBy(1);
		seekbar.setMax(30);
		seekbar2.setProgress(1);
		seekbar2.incrementProgressBy(1);
		seekbar2.setMax(86399);
		seekbar.setProgress(progress);
		seekbar2.setProgress(progress2);
		value.setText("Frequency is: "+progress+" sec");
		int hour= progress2 / 3600;
		int app = progress2 - hour*3600;
		int min = app / 60;
		int sec = app - min*60;
		value2.setText("Timing is: "+hour +"h "+min +"min e "+sec+"sec");
		final Button button = (Button) findViewById(R.id.button);
		final Button button2 = (Button) findViewById(R.id.button_reset);
		
		/**
		* Catch the new value of the seekbar
		*/
		seekbar.setOnSeekBarChangeListener( new OnSeekBarChangeListener()
			{
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
				{
				value.setText("Frequency is: "+progress+"sec");
				}
			public void onStartTrackingTouch(SeekBar seekBar)
				{
				}
			public void onStopTrackingTouch(SeekBar seekBar)
				{
				}	
			});

		/**
		* Catch the new value of the seekbar2
		*/
		seekbar2.setOnSeekBarChangeListener( new OnSeekBarChangeListener()
			{
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
				{
				int hour= progress / 3600;
				int app = progress - hour*3600;
				int min = app / 60;
				int sec = app - min*60;
				value2.setText("Timing is: "+hour +"h "+min +"min e "+sec+"sec");
				}
			public void onStartTrackingTouch(SeekBar seekBar)
				{
				}
			public void onStopTrackingTouch(SeekBar seekBar)
				{
				}	
			});
		
		/**
		* Set the choose values
		*/
		button.setOnClickListener(new View.OnClickListener() {	
			@Override
			public void onClick(View v) {
				long p = seekbar.getProgress();
				long q = seekbar2.getProgress();

				Intent i = new Intent(SecondActivity.this, BluetoothMainActivity.class);
				i.putExtra("numbers", p);
				i.putExtra("numbers2", q);
				startActivity(i); 
			}
		}); 

		
		/**
		* Reset of the values
		*/
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				final ProgressDialog myPd_ring = ProgressDialog.show(SecondActivity.this, "Please wait", "Resetting values..", true);
				myPd_ring.setCancelable(true);

				new Thread(new Runnable() { 
					public void run() {
						try
						{
							Thread.sleep(750);
							int progress = 1; 
							seekbar.setProgress(progress);
							seekbar2.setProgress(progress);
						}catch(Exception e){}
						myPd_ring.dismiss();
					}
				}).start(); 
			}
		}); 
	}
}