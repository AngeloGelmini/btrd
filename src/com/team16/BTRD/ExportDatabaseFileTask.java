package com.team16.BTRD;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;


class ExportDatabaseFileTask extends AsyncTask<String, Void, Boolean> 
    {
	/**
	* Async task that perform the exporting if the search on the sdcard
	* @param args string that contain the name of the new database 
	*/
        protected Boolean doInBackground(final String... args) 
        {
        	// defining the directory and the name of the file
            File dbFile = new File(Environment.getDataDirectory() + "/data/com.team16.BTRD/databases/" + args[0]);
            File exportDir = new File( "/sdcard/","");//(Environment.getExternalStorageDirectory(), "");        
            if (!exportDir.exists()) 
            {
                exportDir.mkdirs();
            }

            File file = new File(exportDir, dbFile.getName());
            
            // coping the file
            try 
            {
                file.createNewFile();
                this.copyFile(dbFile, file);
                return true;
            } 
            catch (IOException e) 
            {
                Log.e("mypck", e.getMessage(), e);
                return false;
            }
        }
        
        /**
         * 
         * @param src source file
         * @param dst destination file
         * @throws IOException
         */
        void copyFile(File src, File dst) throws IOException 
        {
            FileChannel inChannel = new FileInputStream(src).getChannel();
            FileChannel outChannel = new FileOutputStream(dst).getChannel();
            try 
            {
                inChannel.transferTo(0, inChannel.size(), outChannel);
            } 
            finally 
            {
                if (inChannel != null)
                    inChannel.close();
                if (outChannel != null)
                    outChannel.close();
            }
        }
    }