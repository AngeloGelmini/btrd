package com.team16.BTRD;

import java.io.IOException;

import android.bluetooth.BluetoothSocket;

/**
 * this class perform the reconnection at the socket
 */
public class Reconnect extends Thread {
	 	
		/**
	    * Perform the reconnection
	    *     
	    * @param mmSocket the socket that need to be reopened
	    */
		Reconnect(final BluetoothSocket mmSocket){
			int i=0;
			boolean running =true;
			while (running){
				try {
	        		Thread.sleep(10000L); // each 10 second try to reconnect
	        		}	
	        		catch (Exception exception) {
	        			exception.printStackTrace();
	        			}
				try {
					mmSocket.connect();
					return;
					} catch (IOException e) {
						e.printStackTrace();
					}
				i++;
				if(i*10000 > 300000) { // for 5 minute it tries to reconnect
					running=false;
				}
			}
			return;
		}


}