package com.team16.BTRD;


import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the main Activity that displays the current session.
 */
public class BluetoothMainActivity extends Activity implements OnClickListener {
    // Debugging
    private static final String TAG = "BluetoothSession";
    private static final boolean D = true;

    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    
    // Layout Views
    private Button mSendButton;
    private Button mExportButton;
    private TextView data;
    private TextView data2;
    
    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the services
    private BluetoothService mBluetService = null;
    private static String address;
    
    // Declaration of system variables
    private static long temp = 0;
    private static long temp2 = 0;
    private static boolean control=true;
    
    // database declaration
    MyDatabase db=new MyDatabase(this, "Database.db");

    // Diagram
    DiagramDesign dg;
    
    // power management
    private PowerManager.WakeLock mWakeLock;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(D) Log.e(TAG, "+++ ON CREATE +++");
        // Set up the window layout
        setContentView(R.layout.main);
        
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "My Tag");
        mWakeLock.acquire();

        // Diagram initialization
        dg=new DiagramDesign(BluetoothMainActivity.this);
        // Set the BUTTON SETTINGS
        mSendButton = (Button) findViewById(R.id.button_send);
        mSendButton.setOnClickListener(this);
        
        // Initialization of the freq & time value
        Intent intent = getIntent();
        temp = intent.getLongExtra("numbers", 1);
        data = (TextView) findViewById(R.id.textview); 
        data.setText("receiving frequency: "+temp+" sec");
        Intent intent2 = getIntent();
        temp2 = intent2.getLongExtra("numbers2", 1);
        data2 = (TextView) findViewById(R.id.textview2);
        int hour= (int)temp2 / 3600;
        int app = (int)temp2 - hour*3600;
        int min = app / 60;
		int sec = app - min*60;
		data2.setText("receiving time: "+hour +"h "+min +"min e "+sec+"sec");
        
        // Set export button with a progress bar
        mExportButton=(Button) findViewById(R.id.export);
        mExportButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	final ProgressDialog myPd_ring = ProgressDialog.show(BluetoothMainActivity.this, "Please wait", "Exporting...", true);
        		myPd_ring.setCancelable(true);
        		new Thread(new Runnable() { 
        			public void run() {
        				try
                  	  	{	
        					String appoggio=mConnectedDeviceName + "_data.db";
        					MyDatabase app = new MyDatabase(getApplicationContext(), appoggio);
        					db.open();
        					db.WriteSelectData(app, mConnectedDeviceName);
        					new ExportDatabaseFileTask().execute(appoggio);
                  	  	} catch(Exception ex)
                  	  			{
                  	  			Log.e("Error in Main",ex.toString());
                  	  			}
        				db.close();
        				myPd_ring.dismiss();
        			}
        		}).start();
            }
        });

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }

	@Override
    public void onStart() {
        super.onStart();
        if(D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupApp() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        // Otherwise, setup the session
        } else {
            if (mBluetService == null) setupApp();
        }
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        if(D) Log.e(TAG, "+ ON RESUME +");

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mBluetService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mBluetService.getState() == BluetoothService.STATE_NONE) {
              // Start the Bluetooth services
              mBluetService.start();
            }
        }
    }

    @Override
 	public void onClick(View arg0) {
    	 if(arg0.getId() == R.id.button_send){
    		//define a new Intent for the second Activity
  			Intent intent = new Intent(this,SecondActivity.class);
  			//start the second Activity
  			this.startActivity(intent);
   	      Log.d(TAG, "setupApp()");
   	      return;
    	 }
 		}
    
	private void setupApp() {
        Log.d(TAG, "setupApp()");
        // Initialize the array adapter for the conversation thread
        mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.message);
        // Initialize the BluetoothService to perform bluetooth connections
        mBluetService = new BluetoothService(this, mHandler);
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
        if(D) Log.e(TAG, "- ON PAUSE -");
    }

    @Override
    public void onStop() {
        super.onStop();
        if(D) Log.e(TAG, "-- ON STOP --");
    }

	@Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        mWakeLock.release();
        if (mBluetService != null) mBluetService.stop();
        if(D) Log.e(TAG, "--- ON DESTROY ---");
    }

	private void ensureDiscoverable() {
        if(D) Log.d(TAG, "ensure discoverable");
        if (mBluetoothAdapter.getScanMode() !=
            BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }


    /**
     * The Handler that gets information back from the BluetoothService
     */
	private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:
                    //mTitle.setText(R.string.title_connected_to);
                    //mTitle.append(mConnectedDeviceName);
                    mConversationArrayAdapter.clear();
                    break;
                case BluetoothService.STATE_CONNECTING:
                    //mTitle.setText(R.string.title_connecting);
                    break;
                case BluetoothService.STATE_LISTEN:
                case BluetoothService.STATE_NONE:
                    //mTitle.setText(R.string.title_not_connected);
                    break;
                }
                break;
            case MESSAGE_READ:
            	double freq = BluetoothMainActivity.getFrequency()*1000;
            	double runtime=(System.currentTimeMillis())-BluetoothService.GetTime();
            	int freq_int=(int)(freq/1000);
            	int runtime_int=(int)(runtime/1000);
            	String time = Double.toString(runtime/1000);
            	if ( (runtime_int % freq_int == 0) && (control)){
            		byte[] readBuf = (byte[]) msg.obj;
            		// construct a string from the valid bytes in the buffer
            		String readMessage = PolarMessageParser.parseBuffer(readBuf);
					db.open();  //apriamo il db
                    db.insData(mConnectedDeviceName, time, readMessage);
                    db.close();
                    dg.onDataChanged(readMessage);
            		control=false;
            	}
            	double decimal_part = (runtime/1000) - runtime_int;
            	if ((freq_int == 1)&&(decimal_part >= 0.7)){
        			control=true;
        		}
            	if (runtime_int % freq_int !=0) {control=true;}
                break;
            case MESSAGE_DEVICE_NAME:
                // save the connected device's name
                mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                Toast.makeText(getApplicationContext(), "Connected to "+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                break;
            case MESSAGE_TOAST:
                Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    
    
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
        case REQUEST_CONNECT_DEVICE:
            // When DeviceListActivity returns with a device to connect
            if (resultCode == Activity.RESULT_OK) {
                // Get the device MAC address
                address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                // Get the BLuetoothDevice object
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                // Attempt to connect to the device
                mBluetService.connect(device);
            }
            break;
        case REQUEST_ENABLE_BT:
            // When the request to enable Bluetooth returns
            if (resultCode == Activity.RESULT_OK) {
                // Bluetooth is now enabled, so set up session
                setupApp();
            } else {
                // User did not enable Bluetooth or an error occured
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.scan:
            // Launch the DeviceListActivity to see devices and do scan
            Intent serverIntent = new Intent(this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            return true;
        case R.id.discoverable:
            // Ensure this device is discoverable by others
            ensureDiscoverable();
            return true;
        }
        return false;
    }
    
    public static long getFrequency()
    {
        return temp;
    } 
    public static long getTiming()
    {
        return temp2;
    } 
}