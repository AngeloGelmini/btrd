package com.team16.BTRD;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Define the Class that manage the database generation & usage
 */
public class MyDatabase {

	SQLiteDatabase mDb;
	DbHelper mDbHelper;
	Context mContext;
	private static final int DB_VERSION=1; //Version of the database
	public MyDatabase(Context ctx, String DB_NAME){
			mContext=ctx;
			mDbHelper=new DbHelper(ctx, DB_NAME, null, DB_VERSION);
	}
	
	/**
	* open the database to make it writable and readable
	*/
	public void open(){
		mDb=mDbHelper.getWritableDatabase();
	}
	
	/**
	* close the database
	*/
	public void close(){
		mDb.close();
	}
	
	 /**
	   * perform the inserting of new data
	   *     
	   * @param dev_name name of the device
	   * @param time the runtime of acquisition
	   * @param data the value of the acquisition
	   */ 
	public void insData(String dev_name, String time, String data){
		ContentValues cv=new ContentValues();
		cv.put(DataTable.DEVICE, dev_name);
		cv.put(DataTable.TIMESTAMP, time);
		cv.put(DataTable.READ, data);
		mDb.insert(DataTable.DATA_TABLE, null, cv);
	}
	
	/**
	* method to make a query of all the data
	* @return Cursor of the query
	*/
	public Cursor fetchData(){
		return mDb.query(DataTable.DATA_TABLE, null,null,null,null,null,null);
	}
	
	/**
	* method to make a query of all the data of a selected {sel} device and perform the export of the csv file
	* @param New is the new database that will be created
	* @param sel the selected device
	*/
	public void WriteSelectData(MyDatabase New, String sel){
		String appoggio=sel + "_data.csv";
		String csvHeader= "_id;time_stamp;read_data"+"\n";
		File exportDir = new File( "/sdcard/","");//(Environment.getExternalStorageDirectory(), "");
		String csvValues = "";
		File outFile = new File(exportDir,appoggio);
        FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(outFile);
			BufferedWriter out = new BufferedWriter(fileWriter);
			out.write(csvHeader);
			Cursor c = mDb.query(DataTable.DATA_TABLE, null, "device_name=?", new String[] { sel }, null, null, null);
			while (c.moveToNext()){
				String name = c.getString(c.getColumnIndex("device_name"));
				csvValues = (c.getString(1)) + ";";
				String time = c.getString(c.getColumnIndex("time_stamp"));
				String temp= Double.toString((int)c.getDouble(2));
				csvValues += temp + ";";
				String read = c.getString(c.getColumnIndex("read_data"));
				csvValues += Double.toString(c.getDouble(3))+ "\n";
				out.write(csvValues);
				New.open();
				New.insData(name,time, read);
				New.close();
			}
			c.close();
			out.close();
			} catch (IOException e) {
				e.printStackTrace();
		}
	}
	
	/**
	* definition of the table contents
	*/
	static class DataTable {
		static final String DATA_TABLE = "received_data";
		static final String ID = "_id";
		static final String DEVICE = "device_name";
		static final String TIMESTAMP ="time_stamp";
		static final String READ = "read_data";
	}

	private static final String DATA_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " 
	+ DataTable.DATA_TABLE + " (" 
			+ DataTable.ID + " integer primary key autoincrement, " 
			+ DataTable.DEVICE + " text not null, " 
			+ DataTable.TIMESTAMP + " text not null, "
			+ DataTable.READ + " text not null);";

	/**
	* class that help the creation of the table
	*/
	private class DbHelper extends SQLiteOpenHelper {
		public DbHelper(Context context, String name, CursorFactory factory,int version) {
			super(context, name, factory, version);
		}
		
		//only when the db get created we create the table
		@Override
		public void onCreate(SQLiteDatabase _db) { 
			_db.execSQL(DATA_TABLE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
			//here we put any changes to the database, if in our new version of the app, the db version number changes
		}

	}

}